import marked from "marked";
import moment from "moment/moment";

export function updateObjectInArray(array, comment, indexElement) {
    return array.map((item, index) => {
        if (index !== indexElement) return item;
        return {
            ...item,
            ...comment
        };
    });
}

export function remove(element) {
    return this.state.data.filter(e => e._id !== element);
}

export function findById(element) {
    return this.state.data.find(e => e._id === element);
}


export function date(cts, lang = 'US_us') {
    let rawMarkup = '';
    if(lang === 'US_us') {
        rawMarkup =  marked(moment(cts).format('MM-DD-YYYY').toString());
    } else {
        rawMarkup =  marked(moment(cts).format('DD-MM-YYYY').toString());
    }
    return {__html: rawMarkup};
}

// Textarea lineBreak
export function nl2br (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    let breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}