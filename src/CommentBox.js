//CommentBox.js
import React, {Component} from 'react';
import axios from 'axios';
import {updateObjectInArray, findById, remove} from './Helpers';
import CommentList from './CommentList';
import CommentForm from './CommentForm';
import style from './style';

class CommentBox extends Component {
    constructor(props) {
        super(props);
        this.state = {data: [], dataHtml: [], deleteId: []};
        this.updateObjectInArray = updateObjectInArray;
        this.findById = findById;
        this.remove = remove;
        this.loadCommentsFromServer = this.loadCommentsFromServer.bind(this);
        this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
        this.handleCommentDelete = this.handleCommentDelete.bind(this);
        this.handleCommentUpdate = this.handleCommentUpdate.bind(this);
        this.buildDataHtml = this.buildDataHtml.bind(this);
        this.initDataHtml = this.initDataHtml.bind(this);
        this.deleteDataHtml = this.deleteDataHtml.bind(this);
        this.dataHtml = [];
    }

    componentDidMount() {
        this.loadCommentsFromServer();
    }

    loadCommentsFromServer() {
        axios.get(this.props.url)
            .then(res => {
                this.setState({data: res.data});
                this.setState({dataHtml: this.initDataHtml(res.data)});
            })
    }

    initDataHtml(data) {
        let prependDataHtml = [];
        for (let i = 0; i < data.length; i++) {
            this.buildDataHtml(data[i], data);
            if (data[i].replyId === null) {
                prependDataHtml = [...prependDataHtml, data[i]];
            }
        }
        return prependDataHtml;
    }

    buildDataHtml(comment, data) {
        data.forEach((item) => {
            if (item.replyId) {
                if (comment._id === item.replyId) {
                    if (!comment.sub) {
                        comment.sub = [];
                    }
                    comment.sub = [...comment.sub, item];
                }
            }
        })
    }

    updateDataHtml(data, comment) {
        let update = false;
        data.forEach((item) => {
            if ((item.sub && item.sub.length) && comment._id !== item._id) {
                this.updateDataHtml(item.sub, comment);
            }
            if (comment.replyId && comment._id !== item._id) {
                update = true;
                if (comment.replyId === item._id) {
                    if (!item.sub) {
                        item.sub = [];
                    }
                    item.sub = [...item.sub, comment];
                }
            }
        });
        if (!update) {
            return [...this.state.dataHtml, comment];
        }
        return this.state.dataHtml;
    }

    deleteDataHtml(dataHtml, id, children = false) {
        dataHtml.forEach((item) => {
            let find = false;
            let index = dataHtml.indexOf(item);
            if (item._id === id) {
                find = true;
                dataHtml = [...dataHtml.splice(index, 1)];
            }
            if (children) {
                find = true;
                this.setState((prevState, props) => ({
                    deleteId: [...prevState.deleteId, item._id]
                }));
            }
            if (item.sub && item.sub.length) {
                if (find) {
                    this.deleteDataHtml(item.sub, id, true);
                } else {
                    this.deleteDataHtml(item.sub, id);
                }
            }
        });
        return this.state.dataHtml;
    }

    handleCommentSubmit(comment) {
        comment._id = Date.now();
        comment.date = Date.now();
        this.setState({data: [...this.state.data, comment]});
        axios.post(this.props.url, comment)
            .then(() => {
                this.setState({dataHtml: this.updateDataHtml(this.state.dataHtml, comment)})
            })
            .catch(err => {
                console.error(err);
            });
    }

    handleCommentDelete(id) {
        this.setState({dataHtml: this.deleteDataHtml(this.state.dataHtml, id)}, () => {
            //children
            this.state.deleteId.forEach((childrenId) => {
                axios.delete(`${this.props.url}/${childrenId}`)
                    .then(res => {
                        console.log('Comment deleted');
                    })
                    .catch(err => {
                        console.error(err);
                    });
            });
        });
        axios.delete(`${this.props.url}/${id}`)
            .then(res => {
                console.log('Comment deleted');
            })
            .catch(err => {
                console.error(err);
            });

    }

    handleCommentUpdate(id, comment) {
        this.setState({data: this.updateObjectInArray(this.state.data, comment, this.state.data.indexOf(this.findById(id)))});
        //sends the comment id and new author/text to our api
        axios.put(`${this.props.url}/${id}`, comment)
            .catch(err => {
                console.log(err);
            })
    }


    render() {
        return (
            <div style={style.commentBox}>
                <h2 style={style.title}>Comments:</h2>
                <CommentList
                    onCommentDelete={this.handleCommentDelete}
                    onCommentUpdate={this.handleCommentUpdate}
                    onCommentReply={this.handleCommentSubmit}
                    data={this.state.dataHtml}/>
                <CommentForm onCommentSubmit={this.handleCommentSubmit}/>
            </div>
        )
    }
}

export default CommentBox;