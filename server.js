//server.js
'use strict'
//first we import our dependencies�
let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let Comment = require('./model/comments');
//and create our instances
let app = express();
let router = express.Router();
//set our port to either a predetermined port number if you have set
//it up, or 3001
let port = process.env.API_PORT || 3001;
let hostname = 'localhost';
let urlmongo = "mongodb://localhost/db_comment2";

app.set('port', process.env.PORT || port);
app.set('host', process.env.HOST || hostname);

mongoose.connect(urlmongo);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erreur lors de la connexion'));
db.once('open', function () {
    console.log("Connexion à la base OK");
});


//now we should configure the API to use bodyParser and look for
//JSON data in the request body
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
//To prevent errors from Cross Origin Resource Sharing, we will set
//our headers to allow CORS with middleware like so:
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
//and remove cacheing so we get the most recent comments
    res.setHeader('Cache-Control', 'no-cache');
    next();
});
//now we can set the route path & initialize the API
router.get('/', function (req, res) {
    res.json({message: 'API Initialized!'});
});

//now we can set the route path & initialize the API
router.get('/', function (req, res) {
    res.json({message: 'API Initialized!'});
});
//adding the /comments route to our /api router
router.route('/comments')
//retrieve all comments from the database
    .get(function (req, res) {
        //looks at our Comment Schema
        Comment.find(function (err, comments) {
            if (err)
                res.send(err);
            //responds with a json object of our database comments.
            res.json(comments)
        });
    })
    //post new comment to the database
    .post(function (req, res) {
        let comment = new Comment();
        //body parser lets us use the req.body
        comment._id = req.body._id;
        comment.text = req.body.text;
        comment.author = req.body.author;
        comment.date = req.body.date;
        comment.replyId = (req.body.replyId) ? req.body.replyId : null;
        comment.save(function (err) {
            if (err)
                res.send(err);
            res.json({message: 'Comment successfully added!'});
        });
    });
//Add this after our get and post routes
//Adding a route to a specific comment based on the database ID
router.route('/comments/:comment_id')
    //The put method gives us the chance to update our comment based on
    //the ID passed to the route
    .put(function (req, res) {
        Comment.findById(req.params.comment_id, function (err, comment) {
            if (err)
                res.send(err);
            //setting the new author and text to whatever was changed. If
            //nothing was changed we will not alter the field.
            (req.body.author) ? comment.author = req.body.author : null;
            (req.body.text) ? comment.text = req.body.text : null;
            //save comment
            comment.save(function (err) {
                if (err)
                    res.send(err);
                res.json({message: 'Comment has been updated'});
            });
        });
    })
    //delete method for removing a comment from our database
    .delete(function (req, res) {
        //selects the comment by its ID, then removes it.
        Comment.remove({_id: req.params.comment_id}, function (err, comment) {
            if (err)
                res.send(err);
            res.json({message: 'Comment has been deleted'})
        })
    });


//Use our router configuration when we call /api
app.use('/api', router);
//starts the server and listens for requests
app.listen(port, function () {
    console.log(`api running on port ${port}`);
});